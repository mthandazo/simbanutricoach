import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simbanutricoach/model/auth_module.dart';
import 'screens/splash.dart';
import 'screens/home.dart';
import 'screens/login.dart';

void  main(){
  runApp(MainApp());
//  runApp(new MaterialApp(
//    debugShowCheckedModeBanner: false,
//    theme: ThemeData(
//      primaryColor: Colors.green
//    ),
////    home: HomePage(),
//  home: LoginPage(),
//  ));
}

class MainApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      routes: <String, WidgetBuilder>{
        '/root': (BuildContext context) => new LoginPage(),
        '/main' : (BuildContext context) => new HomePage(),
        '/splash' : (BuildContext context) => new SplashScreen(),
      },
      home: _handleCurrentState(),
    );
  }

  Widget _handleCurrentState(){
    return SplashScreen();
  }

}
