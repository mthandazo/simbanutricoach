import 'dart:async';
import 'dart:io';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:simbanutricoach/model/bmi_model.dart';
import 'package:sqflite/sqflite.dart';

class DBProvider {
  DBProvider._();

  static final DBProvider db = DBProvider._();

  Database _database;

  Future<Database> get database async {
    if (_database != null) return _database;
    // if _database is null we instantiate it
    _database = await initDB();
    return _database;
  }

  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "bmi.db");
    return await openDatabase(path, version: 1, onOpen: (db) {},
        onCreate: (Database db, int version) async {
      await db
          .execute("CREATE TABLE Bmi(id INTEGER PRIMARY KEY, bmi_value TEXT)");
    });
  }

  newBmiValue(BmiModel newbmi) async {
    final db = await database;
    //get the biggest id in the table
    var table = await db.rawQuery("SELECT MAX(id)+1 as id FROM Bmi");
    int id = table.first["id"];
    if (id == null) {
      id = 1;
      print(id);
    }
    print(id);
    //insert to the table using the new id
    var raw = await db.rawInsert(
        "INSERT Into Bmi(id,bmi_value) VALUES (?,?)", [id, newbmi.bmi]);
    return raw;
  }

  updateBmiValue(BmiModel newbmi, int id) async {
    final db = await database;
    var res = await db
        .update("Bmi", newbmi.toMap(), where: "id = ?", whereArgs: [id]);
    return res;
  }

  getBmiValue(String prod_id) async {
    final db = await database;
    var res = await db.query("Bmi", where: "id = ?", whereArgs: [prod_id]);
    if (res != null) {
      return res;
    } else {
      return null;
    }
  }

  Future<List<BmiModel>> getAllBmi() async {
    final db = await database;
    var res = await db.query("Bmi");
    final List<Map<String, dynamic>> maps = res;
    return List.generate(maps.length, (i) {
      return BmiModel(id: maps[i]['id'], bmi: maps[i]['bmi_value']);
    });
  }

  deleteBmi(String id) async {
    final db = await database;
    return db.delete("Bmi", where: "id = ?", whereArgs: [id]);
  }

  deleteAllBmi() async {
    final db = await database;
    db.rawDelete("DELETE * FROM Bmi");
  }
}
