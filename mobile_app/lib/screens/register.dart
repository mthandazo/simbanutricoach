import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:email_validator/email_validator.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simbanutricoach/screens/home.dart';

class Register extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  String _btn_screen = "Register";
  String _message;
  String _progress_message = "Creating Account, please wait...";
  String _email_address;
  String _password;
  bool loading = false;
  final _formKey = GlobalKey<FormState>();
  TextEditingController _email_controller = TextEditingController();
  TextEditingController _password_controller = TextEditingController();
  ProgressDialog progressDialog;

  bool validate_and_save() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void validate_and_submit() async {
    setState(() {
      _message = "";
      loading = true;
    });
    if (validate_and_save()) {
      String _userid;
      try {
        _userid = "";
        setState(() {
          loading = false;
        });
        if (_userid.length > 0 && _userid != null) {
          progressDialog.show();
          progressDialog.hide();
          _message = "Account created successfuly!";
          Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => new HomePage()));
        } else {
          _message = "Sign up failed!";
          _show_error();
        }
      } catch (e) {
        String tmp = e.toString();
        Fluttertoast.showToast(
          msg: "Hello there $tmp",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
        );
        setState(() {
          loading = false;
          _message = e.message;
          _formKey.currentState.reset();
          _show_error();
        });
      }
    }
  }

  void _show_error() async {
    progressDialog.hide();
    Fluttertoast.showToast(
      msg: _message,
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.CENTER,
    );
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height / 3;
    progressDialog = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: true, showLogs: false);
    progressDialog.style(
        message: _progress_message,
        backgroundColor: Colors.white,
        borderRadius: 5.0,
        progressWidget: CircularProgressIndicator(),
        elevation: 10.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 12.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 16.0, fontWeight: FontWeight.w400));
    return Scaffold(
      body: new Stack(
        children: <Widget>[
          Image.asset(
            'welcome_login.jpg',
            fit: BoxFit.fill,
            width: double.infinity,
            height: double.infinity,
          ),
          Container(
            color: Colors.white.withOpacity(0.1),
            width: double.infinity,
            height: double.infinity,
          ),
          new Container(
            alignment: Alignment.topCenter,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(0.0, 60.0, 0.0, 0.0),
              child: new CircleAvatar(
                backgroundColor: Colors.transparent,
                radius: 70.0,
                child: Image.asset('avatar_circle.png'),
              ),
            ),
          ),
          new Center(
            child: new Padding(
              padding: const EdgeInsets.only(top: 200.0),
              child: new Center(
                child: new Form(
                    key: _formKey,
                    child: new ListView(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: new Material(
                            borderRadius: BorderRadius.circular(10.0),
                            color: Colors.white.withOpacity(0.8),
                            elevation: 0.0,
                            //Email address field
                            child: Padding(
                              padding: const EdgeInsets.only(left: 12.0),
                              child: Visibility(
                                visible: true,
                                child: TextFormField(
                                  controller: _email_controller,
                                  maxLines: 1,
                                  keyboardType: TextInputType.emailAddress,
                                  decoration: InputDecoration(
                                    hintText: "jacktorch@example.com",
                                    icon: Icon(Icons.email),
                                  ),
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return "Email cannot be empty!";
                                    } else if (!EmailValidator.validate(
                                        value.toString())) {
                                      return "Please enter a valid email";
                                    }
                                    return "";
                                  },
                                  onSaved: (value) =>
                                  _email_address = value.trim(),
                                ),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: new Material(
                            borderRadius: BorderRadius.circular(10.0),
                            color: Colors.white.withOpacity(0.8),
                            elevation: 0.0,
                            //Password field field
                            child: Padding(
                              padding: const EdgeInsets.only(left: 12.0),
                              child: Visibility(
                                visible: true,
                                child: TextFormField(
                                  maxLines: 1,
                                  controller: _password_controller,
                                  decoration: InputDecoration(
                                    hintText: "*******",
                                    icon: Icon(Icons.lock),
                                  ),
                                  obscureText: true,
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return "Password cannot be empty!";
                                    } else if (value.length < 8) {
                                      return "Please enter a valid password";
                                    }
                                    return "";
                                  },
                                  onSaved: (value) => _password = value.trim(),
                                ),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: new Material(
                            borderRadius: BorderRadius.circular(20.0),
                            color: Colors.white.withOpacity(0.8),
                            elevation: 0.0,
                            //Button
                            child: MaterialButton(
                                minWidth: MediaQuery.of(context).size.width,
                                child: Text(
                                  _btn_screen,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20.0),
                                ),
                                onPressed: () async {
                                  validate_and_submit();
                                }),
                          ),
                        ),
                        Divider(
                          color: Colors.black,
                        ),
                        Visibility(
                          visible: true,
                          child: new Container(
                            child: InkWell(
                              onTap: () {},
                              child: Text("Login instead",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.white70,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20.0)),
                            ),
                          ),
                        )
                      ],
                    )),
              ),
            ),
          )
        ],
      ),
    );
  }
}
