import 'package:flutter/material.dart';
import 'package:flutter_dialogflow/flutter_dialogflow.dart';
import 'package:simbanutricoach/utilities/chatmessage.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
//import 'package:tts/tts.dart';

class Chatbot extends StatefulWidget {
  @override
  _ChatbotState createState() => _ChatbotState();
}

class _ChatbotState extends State<Chatbot> {
  final List<ChatMessage> _messages = <ChatMessage>[];
  final TextEditingController _textController = new TextEditingController();

  Widget _buildTextComposer() {
    return new IconTheme(
      data: new IconThemeData(color: Theme
          .of(context)
          .accentColor),
      child: new Container(
        margin: const EdgeInsets.symmetric(horizontal: 8.0),
        child: new Row(
          children: <Widget>[
            new Flexible(
              child: new TextField(
                controller: _textController,
                onSubmitted: _handleSubmitted,
                decoration:
                new InputDecoration.collapsed(hintText: "Send a message"),
              ),
            ),
            new Container(
              margin: new EdgeInsets.symmetric(horizontal: 4.0),
              child: new IconButton(
                  icon: new Icon(Icons.send),
                  onPressed: () => _handleSubmitted(_textController.text)),
            ),
          ],
        ),
      ),
    );
  }

  void ResponseModel(String txt) async {
    _textController.clear();
//    Dialogflow dialogflow =Dialogflow(token: "api key");
//    AIResponse response = await dialogflow.sendQuery(query);
    String bot_msg = await _BotResponse(txt);
    ChatMessage message = new ChatMessage(
      text: bot_msg,
      name: "Simba",
      type: false,
    );
//    Tts.speak(response.getMessageResponse());
    setState(() {
      _messages.insert(0, message);
    });
  }

  void _handleSubmitted(String text) {
    _textController.clear();
    ChatMessage message = new ChatMessage(
      text: text,
      name: "Me",
      type: true,
    );
    setState(() {
      _messages.insert(0, message);
    });
    ResponseModel(text);
  }

//  Future<String> _makeGetResponse(String user_message) async {
//    // make GET request
//    String url = 'https://jsonplaceholder.typicode.com/posts';
//    final response = await get(url); // sample info available in response
//    int statusCode = response.statusCode;
//    Map<String, String> headers = response.headers;
//    String contentType = headers['content-type'];
//    String json = response.body;
//    // TODO convert json to object...
//    Map<String, dynamic> map = jsonDecode(json);
//    return map['text'];
//  }

  Future<String> _BotResponse(String msg) async {
    // set up POST request arguments
    String url = 'https://d1205641.ngrok.io/webhooks/rest/webhook';
    Map data = {'sender':"Rasa", 'message': msg};
    var body = json.encode(data);
    print(body);
    var response = await http.post(url,
        headers: {"Content-Type": "application/json"},
        body: body
    );
    var resp_body = response.body;
    List<dynamic> response_list = jsonDecode(resp_body);
    print(response_list[0]['text']);
    return response_list[0]['text'];
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(
          "Simba Nutri Assistant",
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: Colors.green,
      ),
      body: new Column(children: <Widget>[
        new Flexible(
            child: new ListView.builder(
              padding: new EdgeInsets.all(8.0),
              reverse: true,
              itemBuilder: (_, int index) => _messages[index],
              itemCount: _messages.length,
            )),
        new Divider(height: 1.0),
        new Container(
          decoration: new BoxDecoration(color: Theme
              .of(context)
              .cardColor),
          child: _buildTextComposer(),
        ),
      ]),
    );
  }
}
