import 'package:flutter/material.dart';
import 'package:simbanutricoach/screens/home.dart';
import 'package:cached_network_image/cached_network_image.dart';

class ReadArticle extends StatefulWidget {
  final author;
  final headline;
  final body;
  final category;
  final pid;
  final image;
  final likes;
  final source;
  final link;
  final pdate;

  ReadArticle({
    this.author,
    this.category,
    this.body,
    this.pid,
    this.pdate,
    this.source,
    this.link,
    this.likes,
    this.headline,
    this.image
  });

  @override
  _ReadArticleState createState() => _ReadArticleState();
}

class _ReadArticleState extends State<ReadArticle> {
  String bid;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        elevation: 0.1,
        backgroundColor: Colors.green,
        title: InkWell(
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (content) => new HomePage()));
            },
            child: Text("Simba Nutri Coach")),
        actions: <Widget>[
          new IconButton(
              icon: Icon(
                Icons.home,
                color: Colors.white,
              ),
              onPressed: () {}),
        ],
      ),
      body: new ListView(
        children: <Widget>[
          new Container(
            height: 300.0,
            child: GridTile(
              child: Container(
                color: Colors.white70,
                child: new Image(
                  image:
                  new CachedNetworkImageProvider(widget.image),
                  fit: BoxFit.cover,
                ),
              ),
              footer: new Container(
                color: Colors.white70,
                child: ListTile(
                  leading: new Text(
                    widget.headline,
                    style:
                    TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0),
                  ),
                ),
              ),
            ),
          ),

          //first buttons
          Row(
            children: <Widget>[
              //the size button
              Expanded(
                child: MaterialButton(
                    onPressed: () {},
                    color: Colors.green,
                    textColor: Colors.white,
                    elevation: 0.2,
                    child: new Text("Read full article")),
              ),

              new IconButton(
                icon: Icon(Icons.thumb_up),
                color: Colors.green,
                onPressed: () {
                  if (bid != null){
//                    _addToCart(
//                        bid,
//                        widget.seller_id.toString(),
//                        widget.prod_detail_name.toString(),
//                        widget.prod_id.toString(),
//                        widget.prod_detail_price.toString(),
//                        widget.prod_detail_image.toString(),
//                        _selectedSize,
//                        _selectedQty,
//                        _selectedColor);
                  }else{
//                    Navigator.push(context, new MaterialPageRoute(builder: (context)=> new Login(
//                      seller_id: widget.seller_id.toString(),
//                      prod_detail_name: widget.prod_detail_name,
//                      prod_id: widget.prod_id,
//                      prod_detail_price: widget.prod_detail_price,
//                      prod_detail_image: widget.prod_detail_image,
//                      siz: _selectedSize,
//                      qt: _selectedQty,
//                      col: _selectedColor,
//                    )));
                  }
                },
              ),

              new IconButton(
                icon: Icon(Icons.comment),
                color: Colors.green,
                onPressed: () {
                  if (bid != null){
//                    _addToFavs(
//                        bid,
//                        widget.seller_id,
//                        widget.prod_detail_name,
//                        widget.prod_id,
//                        widget.prod_detail_price,
//                        widget.prod_detail_image,
//                        widget.prod_detail_brand,
//                        widget.prod_detail_cond,
//                        widget.prod_detail_oldp,
//                        widget.prod_detail);
                  }else{
//                    Fluttertoast.showToast(
//                        msg: "Oops seems like you are using a guest account, go to account and sign in first",
//                        toastLength: Toast.LENGTH_LONG,
//                        gravity: ToastGravity.CENTER,
//                        timeInSecForIos: 2);
                  }
                },
              ),

              new IconButton(
                icon: Icon(Icons.share),
                color: Colors.green,
                onPressed: () {
                  if (bid != null){
//                    _addToFavs(
//                        bid,
//                        widget.seller_id,
//                        widget.prod_detail_name,
//                        widget.prod_id,
//                        widget.prod_detail_price,
//                        widget.prod_detail_image,
//                        widget.prod_detail_brand,
//                        widget.prod_detail_cond,
//                        widget.prod_detail_oldp,
//                        widget.prod_detail);
                  }else{
//                    Fluttertoast.showToast(
//                        msg: "Oops seems like you are using a guest account, go to account and sign in first",
//                        toastLength: Toast.LENGTH_LONG,
//                        gravity: ToastGravity.CENTER,
//                        timeInSecForIos: 2);
                  }
                },
              ),

            ],
          ),

          Divider(color: Colors.green),

          new ListTile(
            title: new Text("Article Summary"),
            subtitle: new Text(widget.body),
          ),

          Divider(color: Colors.green),

          new Row(
            children: <Widget>[
              new Padding(
                padding: const EdgeInsets.fromLTRB(12.0, 5.0, 5.0, 5.0),
                child: new Text(
                  "Author",
                  style: TextStyle(color: Colors.green),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(5.0),
                child: new Text(widget.author),
              )
            ],
          ),

          new Row(
            children: <Widget>[
              new Padding(
                padding: const EdgeInsets.fromLTRB(12.0, 5.0, 5.0, 5.0),
                child: new Text(
                  "Date",
                  style: TextStyle(color: Colors.green),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(5.0),
                child: new Text(widget.pdate),
              )
            ],
          ),

          new Row(
            children: <Widget>[
              new Padding(
                padding: const EdgeInsets.fromLTRB(12.0, 5.0, 5.0, 5.0),
                child: new Text(
                  "Category",
                  style: TextStyle(color: Colors.green),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(5.0),
                child: new Text(widget.category),
              )
            ],
          ),

          //similar products section

          Divider(color: Colors.brown),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: new Text("Recent Comments", style: TextStyle(color: Colors.green),),
          ),

//          new Container(
//            height: 260.0,
//            child: Products(prod_category: widget.prod_detail_cond),
//          )
        ],
      ),
    );
  }
}
