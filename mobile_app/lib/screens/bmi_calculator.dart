import 'package:flutter/material.dart';
import 'dart:math';
import 'package:fl_chart/fl_chart.dart';
import 'package:simbanutricoach/db/db_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simbanutricoach/model/bmi_model.dart';

enum Page { calculator, statistics }

class BmiDashboard extends StatefulWidget {
  @override
  _BmiDashboardState createState() => _BmiDashboardState();
}

class _BmiDashboardState extends State<BmiDashboard> {
  List<BmiModel> _bmi_values = <BmiModel>[];
  Page _selectedPage = Page.calculator;
  MaterialColor active = Colors.red;
  MaterialColor notActive = Colors.grey;
  List start_Group ;
  List bar_Group;
  SharedPreferences sharedPreferences;
  String weight, height;
  String report = " ";
  String bmi_current = "0.0";
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController _weight = TextEditingController();
  TextEditingController _height = TextEditingController();

//  @override
//  void initState() {
//    // TODO: implement initState
//    super.initState();
//    _calculateGroup();
//  }

  void _loadBmi() async {
    var temp = await DBProvider.db.getAllBmi();
    setState(() {
      _bmi_values = temp;
    });
  }

  void _AddBmi(String value) async {
    await DBProvider.db.newBmiValue(new BmiModel(bmi: value));
  }

  void _RemoveBmi() async {
    await DBProvider.db.deleteAllBmi();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: new AppBar(
          title: Row(
            children: <Widget>[
              Expanded(
                  child: FlatButton.icon(
                      onPressed: () {
                        setState(() => _selectedPage = Page.calculator);
                      },
                      icon: Icon(
                        Icons.dashboard,
                        color: _selectedPage == Page.calculator
                            ? active
                            : notActive,
                      ),
                      label: Text('BMI CALC'))),
              Expanded(
                  child: FlatButton.icon(
                      onPressed: () {
                        setState(() => _selectedPage = Page.statistics);
                      },
                      icon: Icon(
                        Icons.sort,
                        color: _selectedPage == Page.statistics
                            ? active
                            : notActive,
                      ),
                      label: Text('Statistics'))),
            ],
          ),
          elevation: 0.0,
          backgroundColor: Colors.white,
        ),
        body: _loadScreen());
  }

  Widget _loadScreen() {
    switch (_selectedPage) {
      case Page.calculator:
        return Column(
          children: <Widget>[
            ListTile(
              subtitle: FlatButton.icon(
                onPressed: null,
                icon: Icon(
                  Icons.ac_unit,
                  size: 30.0,
                  color: Colors.green,
                ),
                label: Text(bmi_current,
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 30.0, color: Colors.green)),
              ),
              title: Text(
                'BMI',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 24.0, color: Colors.grey),
              ),
            ),
            Expanded(
                child: Form(
                    key: _formKey,
                    child: new ListView(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(12.0),
                          child: Text(report),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(12.0),
                          child: TextFormField(
                              controller: _height,
                              decoration:
                                  InputDecoration(hintText: "Height (cm)"),
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'You must enter height';
                                } else if (value.length > 10) {
                                  return 'Invalid height value';
                                }
                              },
                              onSaved: (value) => weight = value.trim()),
                        ),

                        //poduct details
                        Padding(
                          padding: const EdgeInsets.all(12.0),
                          child: TextFormField(
                              controller: _weight,
                              decoration:
                                  InputDecoration(hintText: "Weight (kg)"),
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'You must enter weight';
                                } else if (value.length > 250) {
                                  return 'Invalid weight value';
                                }
                              },
                              onSaved: (value) => height = value.trim()),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Material(
                              borderRadius: BorderRadius.circular(20.0),
                              color: Colors.green,
                              elevation: 0.0,
                              child: MaterialButton(
                                onPressed: () async {
                                  _calculateBmi();
                                },
                                minWidth: MediaQuery.of(context).size.width,
                                child: Text(
                                  "Calculate",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20.0),
                                ),
                              )),
                        ),
                      ],
                    )))
          ],
        );
        break;
      case Page.statistics:
        return Padding(
          padding: const EdgeInsets.all(12.0),
          child: AspectRatio(
            aspectRatio: 1.7,
            child: Card(
              elevation: 0,
              shape:
                  RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
              color: const Color(0xff2c4260),
              child: BarChart(
                BarChartData(
                  alignment: BarChartAlignment.spaceAround,
                  maxY: 60,
                  barTouchData: BarTouchData(
                    enabled: false,
                    touchTooltipData: BarTouchTooltipData(
                      tooltipBgColor: Colors.transparent,
                      tooltipPadding: const EdgeInsets.all(0),
                      tooltipBottomMargin: 8,
                      getTooltipItem: (
                        BarChartGroupData group,
                        int groupIndex,
                        BarChartRodData rod,
                        int rodIndex,
                      ) {
                        return BarTooltipItem(
                          rod.y.round().toString(),
                          TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        );
                      },
                    ),
                  ),
                  titlesData: FlTitlesData(
                    show: true,
                    bottomTitles: SideTitles(
                      showTitles: true,
                      textStyle: TextStyle(
                          color: const Color(0xff7589a2),
                          fontWeight: FontWeight.bold,
                          fontSize: 14),
                      margin: 20,
                      getTitles: (double value) {
                        switch (value.toInt()) {
                          case 0:
                            return 'Mn';
                          case 1:
                            return 'Te';
                          case 2:
                            return 'Wd';
                          case 3:
                            return 'Tu';
                          case 4:
                            return 'Fr';
                          case 5:
                            return 'St';
                          case 6:
                            return 'Sn';
                          default:
                            return '';
                        }
                      },
                    ),
                    leftTitles: const SideTitles(showTitles: false),
                  ),
                  borderData: FlBorderData(
                    show: false,
                  ),
                  barGroups: [
                    BarChartGroupData(
                        x: 0,
                        barRods: [BarChartRodData(y: 18.5, color: Colors.lightBlueAccent)],
                        showingTooltipIndicators: [0]),
                    BarChartGroupData(
                        x: 0,
                        barRods: [BarChartRodData(y: 24.5, color: Colors.lightBlueAccent)],
                        showingTooltipIndicators: [0]),
                    BarChartGroupData(
                        x: 0,
                        barRods: [BarChartRodData(y: 25, color: Colors.lightBlueAccent)],
                        showingTooltipIndicators: [0]),
                    BarChartGroupData(
                        x: 0,
                        barRods: [BarChartRodData(y: 27.3, color: Colors.lightBlueAccent)],
                        showingTooltipIndicators: [0]),
                    BarChartGroupData(
                        x: 0,
                        barRods: [BarChartRodData(y: 35.0, color: Colors.lightBlueAccent)],
                        showingTooltipIndicators: [0]),
                    BarChartGroupData(
                        x: 0,
                        barRods: [BarChartRodData(y: 27.4, color: Colors.lightBlueAccent)],
                        showingTooltipIndicators: [0])
                  ]
                ),
              ),
            ),
          ),
        );
        break;
      default:
        return Container();
    }
  }

  bool isNumeric(String s) {
    if(s == null) {
      return false;
    }
    return double.parse(s, (e) => null) != null;
  }

  void _calculateGroup() async {
    _loadBmi();
    double y_val = 0.0;
    for (var i = 0; i < _bmi_values.length; i++) {
      if (isNumeric(_bmi_values[i].bmi)){
        y_val = double.parse(_bmi_values[i].bmi);
      }
      start_Group = [
        BarChartGroupData(
            x: 0,
            barRods: [BarChartRodData(y: y_val, color: Colors.lightBlueAccent)],
            showingTooltipIndicators: [0])
      ];
      bar_Group.add(start_Group);
      y_val = 0.0;
    }
  }

  void _calculateBmi() async {
    double user_h, h, w, bmi, bmi_diff;
    String temp_error;
    FormState formState = _formKey.currentState;
    if (formState.validate()) {
      height = _height.text;
      weight = _weight.text;
      h = (double.parse(height)) / 100;
      w = double.parse(weight);
      user_h = h * h;
      bmi = w / user_h;
      if (bmi < 18.5) {
        temp_error =
            "Your current BMI stands at ${bmi.toStringAsFixed(2)}. This is considered as underweight.";
      } else if (18.5 < bmi && bmi < 24.9) {
        temp_error =
            "Your current BMI stands at ${bmi.toStringAsFixed(2)}. This is considered as normal weight.";
      } else if (25 < bmi && bmi < 29.9) {
        temp_error =
            "Your current BMI stands at ${bmi.toStringAsFixed(2)}. This considered as overweight.";
      } else if (bmi > 30) {
        temp_error =
            "Your current BMI stands at ${bmi.toStringAsFixed(2)}. This considered as obesity.";
      }
      bmi = roundDouble(bmi, 2);
      bmi_current = bmi.toStringAsFixed(2);
      setState(() {
        report = temp_error;
      });
      await sharedPreferences.setDouble("bmi_value", bmi);
      _AddBmi(bmi_current);
//      _calculateGroup();
    }
  }

  double roundDouble(double value, int places) {
    double mod = pow(10.0, places);
    return ((value * mod).round().toDouble() / mod);
  }
}
