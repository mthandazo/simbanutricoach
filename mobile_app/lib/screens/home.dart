import 'package:flutter/material.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simbanutricoach/components/articles_component.dart';
import 'package:simbanutricoach/components/brainboost.dart';
import 'package:simbanutricoach/screens/bmi_calculator.dart';
import 'package:simbanutricoach/screens/chatbot.dart';
//my own imports

class HomePage extends StatefulWidget {
  final SharedPreferences prefs;

  HomePage({this.prefs});

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    Widget image_carousel = new Container(
      height: 200.0,
      child: new Carousel(
        boxFit: BoxFit.cover,
        images: [
          AssetImage("assets/slider/slider_1.jpg"),
          AssetImage("assets/slider/slide_2.jpg"),
          AssetImage("assets/slider/slider_3.jpeg"),
          AssetImage("assets/slider/slid_4.jpg"),
          AssetImage("assets/slider/slider_5.jpeg")
        ],
        autoplay: false,
        animationCurve: Curves.fastOutSlowIn,
        animationDuration: Duration(milliseconds: 1000),
        dotSize: 4.0,
        indicatorBgPadding: 2.0,
        dotBgColor: Colors.transparent,
      ),
    );
    return Scaffold(
      appBar: new AppBar(
        elevation: 0.0,
        backgroundColor: Colors.green,
        title: Text(
          "Simba Nutri Coach",
          style: TextStyle(color: Colors.white),
        ),
        actions: <Widget>[
          new IconButton(
              icon: Icon(
                Icons.chat_bubble,
                color: Colors.white,
              ),
              onPressed: () {
                Navigator.push(context,
                    new MaterialPageRoute(builder: (context) => new Chatbot()));
              }),
          new IconButton(
              icon: Icon(
                Icons.error_outline,
                color: Colors.white,
              ),
              onPressed: () {
                _showDialog(
                    "Simba Nutri Coach, developed by Dr. Torbet Mucheri, Dr. Kennedy Mubaiwa, Prince Abudu and Mthandazo Ndhlovu for WHO Challenge");
              }),
        ],
      ),
      drawer: new Drawer(
        child: new ListView(
          children: <Widget>[
            //header part
            new UserAccountsDrawerHeader(
              accountName: Text("Mthandazo"),
              accountEmail: Text("mndhlovu@gmail.com"),
              currentAccountPicture: GestureDetector(
                child: new CircleAvatar(
                  backgroundColor: Colors.grey,
                  child: Icon(
                    Icons.person,
                    color: Colors.white,
                  ),
                ),
              ),
              decoration: new BoxDecoration(color: Colors.greenAccent),
            ),

            //body
            InkWell(
              onTap: () {},
              child: ListTile(
                title: Text("Home Page"),
                leading: Icon(Icons.home, color: Colors.green),
              ),
            ),

            InkWell(
              onTap: () {
                Navigator.push(
                    context,
                    new MaterialPageRoute(
                        builder: (context) => new BmiDashboard()));
              },
              child: ListTile(
                title: Text("BMI"),
                leading: Icon(Icons.assessment, color: Colors.green),
              ),
            ),

            InkWell(
              onTap: () {},
              child: ListTile(
                title: Text("Study Room"),
                leading: Icon(Icons.school, color: Colors.green),
              ),
            ),

            InkWell(
              onTap: () {},
              child: ListTile(
                title: Text("Personal Assistant"),
                leading: Icon(
                  Icons.shop,
                  color: Colors.green,
                ),
              ),
            ),

            Divider(),

            InkWell(
              onTap: () {},
              child: ListTile(
                title: Text("Settings"),
                leading: Icon(Icons.settings, color: Colors.green),
              ),
            ),

            InkWell(
              onTap: () async {
//                _signOut();
              },
              child: ListTile(
                title: Text("Share"),
                leading: Icon(
                  Icons.share,
                  color: Colors.green,
                ),
              ),
            ),
          ],
        ),
      ),
      body: new ListView(
        children: <Widget>[
          //image carousel
          image_carousel,
          //padding
          new Padding(
            padding: const EdgeInsets.all(8.0),
            child: new Text("Brain Boost"),
          ),
          //horizontal listview
          BrainBoost(),

          //padding widget
          new Padding(
            padding: const EdgeInsets.all(20.0),
            child: new Text('Learning Resources'),
          ),

          //grid view
          Container(
            height: 320.0,
            child: ArcticlesComponent(article_category: "Nutrition Articles"),
          )
        ],
      ),
    );
  }

  void _showDialog(String s) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text("Simba Nutri Coach"),
          content: new Text(s),
          actions: <Widget>[
            new FlatButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: new Text("OK")),
          ],
        );
      },
    );
  }

//  Future _signOut() async{
//    await FirebaseAuth.instance.signOut();
//    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=> new Login()));
//  }
}
