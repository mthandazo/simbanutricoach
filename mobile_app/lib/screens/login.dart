import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simbanutricoach/model/auth_module.dart';
import 'package:simbanutricoach/screens/home.dart';
import 'package:simbanutricoach/screens/register.dart';

class LoginPage extends StatefulWidget {

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  String _btn_screen = "Login";
  String _message;
  String _progress_message = "Signing in, please wait..";
  String _email_address;
  String _password;
  bool loading = false;
  bool _vfcode = false;
  bool _phcode = true;
  final _formKey = GlobalKey<FormState>();
  BaseAuth auth;
  VoidCallback login_callback;
  SharedPreferences prefs;
  TextEditingController _email_controller = TextEditingController();
  TextEditingController _password_controller = TextEditingController();
  ProgressDialog progressDialog;

  bool validate_and_save() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void validate_and_submit() async {
    setState(() {
      _message = "";
      loading = true;
    });
    if (validate_and_save()) {
      String _userid;
      try {
        if (_email_address == "test@gmail.com" && _password == "123456789"){
          _userid = _email_address;
        }
//        _userid = await auth.sign_in(_email_address, _password);
        setState(() {
          loading = false;
        });
        if (_userid.length > 0 && _userid != null) {
//          login_callback();
          progressDialog.hide();
          _updateSharePref(_userid);
          _message = "Login was successful, welcome back!";
          Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => new HomePage()));
        } else {
          _message = "Sign in failed!";
          _show_error();
        }
      } catch (e) {
        setState(() {
          loading = false;
          _message = e.message;
          _formKey.currentState.reset();
          _show_error();
        });
      }
    }
  }

  void _show_error() async {
    progressDialog.hide();
    Fluttertoast.showToast(
      msg: _message,
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.CENTER,
    );
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height / 3;
    progressDialog = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: true, showLogs: false);
    progressDialog.style(
        message: _progress_message,
        backgroundColor: Colors.white,
        borderRadius: 5.0,
        progressWidget: CircularProgressIndicator(),
        elevation: 10.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 12.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 16.0, fontWeight: FontWeight.w400));
    return Scaffold(
      body: new Stack(
        children: <Widget>[
          Image.asset(
            'assets/slider/lgn_back_3.jpg',
            fit: BoxFit.fill,
            width: double.infinity,
            height: double.infinity,
          ),
          Container(
            color: Colors.white.withOpacity(0.1),
            width: double.infinity,
            height: double.infinity,
          ),
          new Container(
            alignment: Alignment.topCenter,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(0.0, 60.0, 0.0, 0.0),
              child: new CircleAvatar(
                backgroundColor: Colors.transparent,
                radius: 70.0,
                child: Image.asset('assets/avatar_circle.png'),
              ),
            ),
          ),
          new Center(
            child: new Padding(
              padding: const EdgeInsets.only(top: 200.0),
              child: new Center(
                child: new Form(
                    key: _formKey,
                    child: new ListView(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: new Material(
                            borderRadius: BorderRadius.circular(10.0),
                            color: Colors.white.withOpacity(0.8),
                            elevation: 0.0,
                            //Email address field
                            child: Padding(
                              padding: const EdgeInsets.only(left: 12.0),
                              child: Visibility(
                                visible: _phcode,
                                child: TextFormField(
                                  controller: _email_controller,
                                  maxLines: 1,
                                  keyboardType: TextInputType.emailAddress,
                                  decoration: InputDecoration(
                                    hintText: "jacktorch@example.com",
                                    icon: Icon(Icons.email),
                                  ),
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return "Email cannot be empty!";
                                    }
                                  },
                                  onSaved: (value) =>
                                  _email_address = value.trim(),
                                ),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: new Material(
                            borderRadius: BorderRadius.circular(10.0),
                            color: Colors.white.withOpacity(0.8),
                            elevation: 0.0,
                            //Password field field
                            child: Padding(
                              padding: const EdgeInsets.only(left: 12.0),
                              child: Visibility(
                                visible: _phcode,
                                child: TextFormField(
                                  maxLines: 1,
                                  controller: _password_controller,
                                  decoration: InputDecoration(
                                    hintText: "*******",
                                    icon: Icon(Icons.lock),
                                  ),
                                  obscureText: true,
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return "Password cannot be empty!";
                                    } else if (value.length < 8) {
                                      return "Please enter a valid password";
                                    }

                                  },
                                  onSaved: (value) => _password = value.trim(),
                                ),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: new Material(
                            borderRadius: BorderRadius.circular(20.0),
                            color: Colors.white.withOpacity(0.8),
                            elevation: 0.0,
                            //Button
                            child: MaterialButton(
                                minWidth: MediaQuery.of(context).size.width,
                                child: Text(
                                  _btn_screen,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20.0),
                                ),
                                onPressed: () async {
                                  progressDialog.show();
                                  validate_and_submit();
                                }),
                          ),
                        ),
                        Divider(
                          color: Colors.black,
                        ),
                        Visibility(
                          visible: true,
                          child: new Container(
                            child: InkWell(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => new Register()));
                              },
                              child: Text("Create Account!",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.white70,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20.0)),
                            ),
                          ),
                        )
                      ],
                    )),
              ),
            ),
          )
        ],
      ),
    );
  }
  void _updateSharePref(String uid) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setBool("active_session", true);
    await preferences.setString("uid", uid);
  }
}


