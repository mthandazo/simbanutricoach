import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:simbanutricoach/components/custom_flat_button.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simbanutricoach/model/auth_module.dart';
import 'login.dart';
import 'package:simbanutricoach/model/splash_model.dart';

class SplashScreen extends StatefulWidget {
  final List<Splash> pages = [
    Splash(
      icon: Icons.school,
      title: "Informative",
      description: "Learn about malnutrition",
    ),
    Splash(
      icon: Icons.android,
      title: "Personal Assistant",
      description:
          "Providing with a personal assistant to guide you towards a health diet",
    ),
    Splash(
        icon: Icons.assessment,
        title: "BMI Tracker",
        description: "Check your diet and BMI"),
    Splash(
        icon: Icons.child_care,
        title: "Childcare",
        description:
            "Verify if the meals you are giving to your child are healthy and won\'t lead to Malnutrition")
  ];

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Swiper.children(
        autoplay: false,
        index: 0,
        loop: false,
        pagination: new SwiperPagination(
          margin: new EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 40.0),
          builder: new DotSwiperPaginationBuilder(
              color: Colors.white70,
              activeColor: Colors.white,
              size: 6.5,
              activeSize: 8.0),
        ),
        control: SwiperControl(
          iconPrevious: null,
          iconNext: null,
        ),
        children: _getPages(context),
      ),
    );
  }

  List<Widget> _getPages(BuildContext context) {
    List<Widget> widgets = [];
    for (int index = 0; index < widget.pages.length; index++) {
      Splash page = widget.pages[index];
      widgets.add(
        new Container(
          color: Colors.green,
          child: ListView(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 70.0),
                child: Icon(
                  page.icon,
                  size: 125.0,
                  color: Colors.white,
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.only(top: 50.0, right: 15.0, left: 15.0),
                child: new Text(
                  page.title,
                  softWrap: true,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.white,
                    decoration: TextDecoration.none,
                    fontSize: 24.0,
                    fontWeight: FontWeight.w700,
                    fontFamily: "OpenSans",
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: new Text(
                  page.description,
                  softWrap: true,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.white,
                    decoration: TextDecoration.none,
                    fontSize: 15.0,
                    fontWeight: FontWeight.w300,
                    fontFamily: "OpenSans",
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: page.extraWidget,
              )
            ],
          ),
        ),
      );
    }
    widgets.add(new Container(
      color: Colors.green,
      child: new Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Icon(
              Icons.account_circle,
              size: 125.0,
              color: Colors.white,
            ),
            Padding(
              padding:
                  const EdgeInsets.only(top: 50.0, right: 15.0, left: 15.0),
              child: new Text(
                "Jump into action right away",
                softWrap: true,
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.white,
                  decoration: TextDecoration.none,
                  fontSize: 24.0,
                  fontWeight: FontWeight.w700,
                  fontFamily: "OpenSans",
                ),
              ),
            ),
            Padding(
              padding:
                  const EdgeInsets.only(top: 20.0, right: 15.0, left: 15.0),
              child: CustomFlatButton(
                title: "Get Started",
                fontSize: 22,
                fontWeight: FontWeight.w700,
                textColor: Colors.white,
                onPressed: () {
                  Navigator.pushReplacement(context,
                      MaterialPageRoute(builder: (context) => new LoginPage()));
                },
                splashColor: Colors.black12,
                borderColor: Colors.white,
                borderWidth: 2,
                color: Colors.green,
              ),
            )
          ],
        ),
      ),
    ));
    return widgets;
  }
}
