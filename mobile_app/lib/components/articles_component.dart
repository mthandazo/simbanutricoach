import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:simbanutricoach/components/single_article.dart';
import 'package:simbanutricoach/model/articles_holder.dart';

class ArcticlesComponent extends StatefulWidget {
  final article_category;

  ArcticlesComponent({this.article_category});

  @override
  _ArcticlesComponentState createState() => _ArcticlesComponentState();
}

class _ArcticlesComponentState extends State<ArcticlesComponent> {
  FirebaseDatabase _database = FirebaseDatabase.instance;
  List<ArticlesHolder> articles_list = <ArticlesHolder>[];

  @override
  void initState() {
    if (widget.article_category.toString() == "general") {
      _database.reference().child("articles").onChildAdded.listen(_childAdded);
      _database
          .reference()
          .child("articles")
          .onChildChanged
          .listen(_childChanged);
      _database
          .reference()
          .child("articles")
          .onChildRemoved
          .listen(_childRemoved);
    } else {
      _database
          .reference()
          .child("articles")
          .orderByChild("category")
          .equalTo(widget.article_category)
          .onChildAdded
          .listen(_childAdded);
      _database
          .reference()
          .child("articles")
          .orderByChild("category")
          .equalTo(widget.article_category)
          .onChildChanged
          .listen(_childChanged);
      _database
          .reference()
          .child("articles")
          .orderByChild("category")
          .equalTo(widget.article_category)
          .onChildRemoved
          .listen(_childRemoved);
    }
  }

  void _childAdded(Event event) {
    setState(() {
      articles_list.add(ArticlesHolder.fromDataSnapshot(event.snapshot));
    });
  }

  void _childChanged(Event event) {
    var changedProd = articles_list.singleWhere((article) {
      return article.key == event.snapshot.key;
    });
    setState(() {
      articles_list[articles_list.indexOf(changedProd)] =
          ArticlesHolder.fromDataSnapshot(event.snapshot);
    });
  }

  void _childRemoved(Event event) {
    var delProd = articles_list.singleWhere((article) {
      return article.key == event.snapshot.key;
    });

    setState(() {
      articles_list.removeAt(articles_list.indexOf(delProd));
    });
  }


  @override
  Widget build(BuildContext context) {
    return GridView.builder(
        itemCount: articles_list.length,
        gridDelegate:
        new SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
        itemBuilder: (BuildContext context, int index) {
          return SingleArticle(
            a_title: articles_list[index].title,
            image: articles_list[index].image,
            headline: articles_list[index].headline,
            body: articles_list[index].body,
            link: articles_list[index].link,
            author: articles_list[index].author,
            source: articles_list[index].source,
            pdate: articles_list[index].pdate,
            pid: articles_list[index].pid,
            category: articles_list[index].category,
          );
        });
  }
}

