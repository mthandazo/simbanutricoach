import 'package:flutter/material.dart';

class BrainBoost extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 120.0,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: <Widget>[
          Category(
            image_location: 'assets/h_view/school.png',
            image_caption: 'Study Room',
          ),

          Category(
            image_location: 'assets/h_view/articles.png',
            image_caption: 'Articles',
          ),

          Category(
            image_location: 'assets/h_view/news.png',
            image_caption: 'News',
          ),

          Category(
            image_location: 'assets/h_view/books.png',
            image_caption: 'Heath Books',
          )
        ],
      ),
    );
  }
}

class Category extends StatelessWidget {
  final String image_location;
  final String image_caption;

  Category({this.image_location, this.image_caption});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(2.0),
      child: InkWell(
        onTap: () {

        },
        child: Container(
          width: 100.0,
          child: ListTile(
            title: Image.asset(
              image_location,
              width: 100.0,
              height: 80.0,
            ),
            subtitle: Container(
              alignment: Alignment.topCenter,
              child: Text(image_caption),
            ),
          ),
        ),
      ),
    );
  }
}
