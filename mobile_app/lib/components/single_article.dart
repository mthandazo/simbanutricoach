import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import '../screens/read_article.dart';

class SingleArticle extends StatelessWidget {
  final a_title;
  final author;
  final headline;
  final image;
  final link;
  final source;
  final pdate;
  final pid;
  final body;
  final category;

  SingleArticle({
    this.a_title,
    this.body,
    this.pdate,
    this.headline,
    this.link,
    this.source,
    this.image,
    this.author,
    this.pid,
    this.category
  });


  @override
  Widget build(BuildContext context) {
    return Card(
      child: Hero(
          tag: pid,
          child: Material(
            child: InkWell(
              onTap: () => Navigator.of(context).push(new MaterialPageRoute(builder: (context)=> new ReadArticle(
                author: author,
                category: category,
                headline: headline,
                body: body,
                pid: pid,
                pdate: pdate,
                source: source,
                link: link,
                image: image,
              ))),
              child: GridTile(
                  footer: Container(
                    color: Colors.white70,
                    child: ListTile(
                      leading: Text(
                        headline,
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      title: new Text(""),
                      subtitle: new Text(""),
                    ),
                  ),
                  child: new Image(
                    image: new CachedNetworkImageProvider(image),
                    fit: BoxFit.cover,
                  )),
            ),
          )),
    );
  }
}
