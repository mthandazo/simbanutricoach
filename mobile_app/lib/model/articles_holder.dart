import 'package:firebase_database/firebase_database.dart';

class ArticlesHolder{


  static const KEY = "key";
  static const PID = "pid";
  static const TITLE = "title";
  static const BODY = "body";
  static const AUTHOR = "author";
  static const LINK = "link";
  static const HEADLINE = "headline";
  static const SOURCE = "source";
  static const IMAGE = "image";
  static const PDATE = "pdate";
  static const CATEGORY = "category";

  String key, pid, title, body, author, link, headline, source, image, pdate, category;

  ArticlesHolder(this.key, this.pid, this.author, this.title, this.body, this.category, this.headline, this.image, this.link, this.pdate, this.source);

  ArticlesHolder.fromDataSnapshot(DataSnapshot snap):
        this.key = snap.key,
        this.pid = snap.value[PID],
        this.author = snap.value[AUTHOR],
        this.image = snap.value[IMAGE],
        this.source = snap.value[SOURCE],
        this.link = snap.value[LINK],
        this.headline = snap.value[HEADLINE],
        this.pdate = snap.value[PDATE],
        this.category = snap.value[CATEGORY],
        this.body = snap.value[BODY];
}