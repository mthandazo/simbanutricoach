import 'package:flutter/material.dart';

class Splash {
  IconData  icon;
  String  title;
  String  description;
  Widget  extraWidget;

  Splash({this.icon, this.title, this.description, this.extraWidget}){
    if (extraWidget == null)
      extraWidget = new Container();
  }
}