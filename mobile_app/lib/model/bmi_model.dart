
import 'dart:convert';

BmiModel clientFromJson(String str) {
  final jsonData = json.decode(str);
  return BmiModel.fromMap(jsonData);
}

String clientToJson(BmiModel data) {
  final dyn = data.toMap();
  return json.encode(dyn);
}

class BmiModel{
  int id;
  String bmi;

  BmiModel({
    this.id,
    this.bmi
  });

  Map<String, dynamic> toMap() =>{
    'id' : id,
    'bmi' : bmi
  };

  factory BmiModel.fromMap(Map<String, dynamic> json) => new BmiModel(
      id: json["id"],
      bmi: json["bmi"]
  );


  clientFromJson(String str) {
    final jsonData = json.decode(str);
    return BmiModel.fromMap(jsonData);
  }

  clientToJson(BmiModel data) {
    final dyn = data.toMap();
    return json.encode(dyn);
  }

}