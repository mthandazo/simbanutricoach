# SIMBA NUTRI COACH

The 2018 Global Nutrition Report reveals that the global burden of malnutrition is unacceptably high and now affects every country in the world. But it also highlights that if we act now it is not too late to end malnutrition in all its forms. According to WHO, today the world faces a double burden of malnutrition that includes both under nutrition and overweight, especially in low- and middle-income countries. About 22% of the world’s 0-59 months children are stunted, 7.5% are wasted and 5.6% are overweight imagine the implications on health and wellbeing of such future leaders. Today’s world is packed with a wide range of diseases affecting mankind and research shows the bulky of such ailments are a result of what we eat. Right from the first 1000 days to the advanced ages of mankind, the need to maintain healthy bodies is more importance than ever before and this places nutrition at a pivotal position. What are the solutions? How best can global citizens be equipped? How can we leverage the disruptive power of technology to enhance mastery of nutrition?  We adopted a multi-disciplinary approach to identify the challenge, scope a solution and put together our minds bringing together medical intellect, IT wizardry, social development thought leadership to hatch ‘SimbaNutriCoach’. This is an all age group APP to help you track your Body Mass Index, equip you with cutting edge learning material on nutrition and assist you in longitudinal tracking of your health and wellbeing with inbuilt capabilities to give end user a great experience. This prototype after full testing and iteration will be the first of its kind to offer tri-lingual( KiSwahili, French and English) user interface to ensure that we reach beyond language barriers and will make the app usable offline as well. Added functionalities to support adherence to fitness initiatives, track critical dates and activities for expecting mothers and present customized analytics to monitor body’s response to dietary changes and season are part of the composite value derivatives. In today’s world of IOT, there is need to leverage technology for enhancing longevity and give customized nutrition management to also manage chronic health conditions.

### Tech

SimbaNutriCoach was built using a number of open source libraries and frameworks such as:

* [Flutter](https://flutter.dev/) - Mobile Application
* [Fl_chart](https://pub.dev/packages/fl_chart) Creating Charts 
* [Rasa](https://rasa.com/) - Conversational Agent
* [Beautiful Soup 4](https://pypi.org/project/beautifulsoup4/) - Extraction of data from web pages.

### Mobile Application Installation
The mobile application can be downloaded from the following [drive link](https://drive.google.com/drive/folders/18LMwc1xRn5vhh8r17ipsC_DIKviA9fA9?usp=sharing). It is compatible with all versions of android above 4.2 (Jelly Bean).

### Running the Chatbot
To run the chatbot from your local linux machine follow the following steps:

```sh
$ apt install python3-dev python3-pip
$ apt install virtualenv
$ virtualenv --python=python3 simba_env 
$ source simba_env/bin/activate
$ git clone https://gitlab.com/mthandazo/simbanutricoach.git
$ cd simbanutricoach
$ pip install -r requirements.txt
$ rasa train
$ rasa shell
```

If you wish to deploy it with docker follow the following steps:

```sh
$ git clone https://gitlab.com/mthandazo/simbanutricoach.git
$ cd simbanutricoach
$ sudo docker-compose up --build
```

Click [here](https://zimcovid19.com/simba-nc) to chat with the bot.


### Acknowledgements
We would like to extend our gratitude to the following:

| Name | Link |
| ------ | ------ |
| UNICEF | https://www.unicef.org/nutrition/ |
| WHO | https://www.who.int/nutrition/globalnutritionreport/en/ |
| Tailored Coaching Method | https://tailoredcoachingmethod.com/7-most-frequently-asked-questions-by-you-on-nutrition/ |
| FAO | https://www.who.int/health-topics/nutrition |
| OPEN | https://www.open.edu/openlearn/health-sports-psychology/health/the-science-nutrition-and-healthy-eating/content-section-overview?active-tab=description-tab |
| Rasa | https://github.com/RasaHQ/rasa-demo |
|Jitesh Gaikwad | https://github.com/JiteshGaikwad |

The media content used inside the application was obtained from image search
engines and we also like to thank the uploaders.


