#!/usr/bin/python
#title			:actions.py
#description	:Custom Jidu Bot actions (fr)
#author			:Mthandazo Ndhlovu
#date			:20191121
#version		:0.1
#notes			:Custom actions
#python_version	:3.6.8
#==============================================================================
from typing import Dict, Text, Any, List, Union, Optional
from rasa_sdk import Action
import time
import datetime
import csv
from rasa_sdk.events import SlotSet
from rasa_sdk.forms import FormAction
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk import Tracker

"""
=======================================
GENERAL AND UTILITY ACTIONS
"""

def bmi_processor(h, w, old_bmi):
    user_h = float(h) / 100
    user_h =  user_h * user_h
    bmi = float(w) / user_h
    bmi = float("{:.2f}".format(bmi))
    if bmi > old_bmi:
        bmi_diff = bmi - old_bmi
        bmi_gradient = f"Note that your BMI has increased from {old_bmi} to {bmi} by {bmi_diff} ."
    elif bmi < old_bmi:
        bmi_diff = old_bmi - bmi
        bmi_gradient = f"Note that your BMI has decreased from {old_bmi} to {bmi} by {bmi_diff} ."
    elif bmi == old_bmi:
        bmi_diff = 0
        bmi_gradient = f"Note that your BMI has not changed the old value {old_bmi}."
    if bmi < 18.5:
        report = f"Your current BMI stands at {bmi}. This is considered as underweight. {bmi_gradient}"
    elif 18.5 < bmi < 24.9:
        report = f"Your current BMI stands at {bmi}. This is considered as normal weight. {bmi_gradient}"
    elif 25 < bmi < 29.9:
        report = f"Your current BMI stands at {bmi}. This considered as overweight. {bmi_gradient}"
    elif bmi > 30:
        report = f"Your current BMI stands at {bmi}. This considered as obesity. {bmi_gradient}"
    return report, bmi



class ActionChitchat(Action):
    """Returns the chitchat utterance dependent on the intent"""

    def name(self) -> Text:
        return "action_chitchat"

    def run(self, dispatcher:CollectingDispatcher, 
            tracker: Tracker, domain:Dict[Text, Any]) -> List[Dict[Text, Any]]:
        intent = tracker.latest_message["intent"].get("name")
        dispatcher.utter_message(template=f"utter_{intent}")
        return []

class ActionBmiTracker(Action):
    """BMI TRACKER"""

    def name(self) -> Text:
        return "action_bmi_tracker"

    def run(self, dispatcher:CollectingDispatcher, 
            tracker: Tracker, domain:Dict[Text, Any]) -> List[Dict[Text, Any]]:
        old_bmi = tracker.get_slot("bmi_current")
        height = tracker.get_slot("height")
        weight = tracker.get_slot("weight")
        report, bmi = bmi_processor(height, weight, old_bmi)
        dispatcher.utter_message(report)
        return [SlotSet('bmi_current',bmi), SlotSet('weight',None), SlotSet('height',None)]

class BmiTrackerForm(FormAction):
    def name(self):
        return "bmi_tracker_form"

    @staticmethod
    def required_slots(tracker:Tracker) -> List[Text]:
        return ["height", "weight"]

    def slot_mappings(self) -> Dict[Text, Union[Dict, List[Dict]]]:
        return {
            "height":self.from_entity(entity="height", intent=["user_height"]),
            "weight":self.from_entity(entity="weight",intent=["user_weight"])
        }

    def validate_height(self, value: Text, dispatcher: CollectingDispatcher,
                                tracker: Tracker, domain: Dict[Text, Any],) -> Dict[Text, Any]:
        """Validate height"""
        if len(str(value)) > 0:
           # convert "out..." to True
            return {"height": value}
        else:
            dispatcher.utter_message(template="utter_wrong_height")
            # validation failed, set slot to None
            return {"height": None}
        
    def validate_weight(self, value: Text, dispatcher: CollectingDispatcher,
                                tracker: Tracker, domain: Dict[Text, Any],) -> Dict[Text, Any]:
        """Validate weight"""
        if len(str(value)) > 0:
            return {"weight": value}     
        else:
            # affirm/deny was picked up as T/F
            dispatcher.utter_message(template="utter_wrong_weight")
            # validation failed, set slot to None
            return {"weight": None}

    def submit(self, dispatcher: CollectingDispatcher,
                tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict]:
        dispatcher.utter_message(template="utter_submit")
        return []

class ActionDefaultAskAffirmation(Action):
    """Asks for an affirmation of the intent if NLU threshold is not met."""

    def name(self) -> Text:
        return "action_default_ask_affirmation"

    def __init__(self) -> None:
        self.intent_mappings = {}
        with open('intent_description_mapping.csv',
                  newline='',
                  encoding='utf-8') as file:
            csv_reader = csv.reader(file)
            for row in csv_reader:
                self.intent_mappings[row[0]] = row[1]

    def run(self,
            dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]
            ) -> List['Event']:

        intent_ranking = tracker.latest_message.get('intent_ranking', [])
        if len(intent_ranking) > 1:
            diff_intent_confidence = (intent_ranking[0].get("confidence") -
                                      intent_ranking[1].get("confidence"))
            if diff_intent_confidence < 0.2:
                intent_ranking = intent_ranking[:2]
            else:
                intent_ranking = intent_ranking[:1]
        first_intent_names = [intent.get('name', '')
                              for intent in intent_ranking
                              if intent.get('name', '') != 'out_of_scope']
        message_title = "Sorry, I'm not sure I've understood " \
                        "you correctly 🤔 Do you mean..."
        mapped_intents = [(name, self.intent_mappings.get(name, name))
                          for name in first_intent_names]
        entities = tracker.latest_message.get("entities", [])
        entities_json, entities_text = get_formatted_entities(entities)
        buttons = []
        for intent in mapped_intents:
            buttons.append({'title': intent[1] + entities_text,
                            'payload': '/{}{}'.format(intent[0],
                                                      entities_json)})
        buttons.append({'title': 'Something else',
                        'payload': '/escape_loop_intent'})
        dispatcher.utter_message(message_title, buttons=buttons)

        return []


def get_formatted_entities(entities: List[Dict[str, Any]]) -> (Text, Text):
    key_value_entities = {}
    for e in entities:
        key_value_entities[e.get("entity")] = e.get("value")
    entities_json = ""
    entities_text = ""
    if len(entities) > 0:
        entities_json = json.dumps(key_value_entities)
        entities_text = ["'{}': '{}'".format(k, key_value_entities[k])
                         for k in key_value_entities]
        entities_text = ", ".join(entities_text)
        entities_text = " ({})".format(entities_text)

    return entities_json, entities_text
"""
END OF GENERAL AND UTILITY ACTIONS
===========================================
START OF GENESIS HARDCODED COURSE ACTIONS
"""
class ActionFaqModel(Action):
    def name(self):
        return "action_faq_model"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker, domain:Dict[Text, Any]) -> List[Dict[Text, Any]]:
        intent_name = tracker.latest_message["intent"]["name"]
        cur_template = "utter_" + intent_name
        dispatcher.utter_message(template=cur_template)
        return []
