FROM python:3.7.4
USER root
SHELL ["/bin/bash", "-c"]
ENV LANG C.UTF
RUN apt-get update -qq && \
    apt-get install -y --no-install-recommends \
    build-essential \
    wget \
    openssh-client \
    graphviz-dev \
    pkg-config \
    git-core \
    openssl \
    libssl-dev \
    libffi6 \
    libffi-dev \
    libpng-dev \
    curl && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* && \
    mkdir /app
WORKDIR /app
COPY requirements.txt ./
RUN pip install -r requirements.txt
COPY . /app
EXPOSE 5055 8080
RUN python -m rasa train
CMD ["/bin/bash", "-c", "./script.sh"]