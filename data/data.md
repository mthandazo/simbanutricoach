## intent:get_started_intent
- restart
- start over
- get started
- please restart
- lets begin
- start high flyers
## intent:greeting_intent
- good morning
- good afternoon
- Hello
- hie simba nutri coach
- simbanutricoach
- Hey
- good evening
- Hi
- whats up
- hello
- heyyy
- good
- hey
- hi

## intent:user_gratitude
- my pleasure
- the pleasure is all mine.
- It's a pleasure for me
- it's my pleasure
- it is my pleasure
- You're welcome.
- No problem.
- Welcome

## intent:thanks_intent
- thanks
- thank you
- thank you every much
- thanks
- thanks a lote
- thanks bot

## intent:goodbye_intent
- goodbye
- see you later
- bye
- chat later
- see you tomorrow
- busy now chat later

## intent:affirm_intent
- yes
- yep
- Yes
- Yep
- yes of course
- why not
- yes please
- okay

## intent:deny_intent
- no
- nop
- hell no
- no way
- i disagree
- Nope
- No

## intent:gen_user_intention
- I have a question
- question please
- excuse me, i have question
- Can you answer my question please
- A question
- I got a question
- one question please
- I dont understand something
- help me understand something
- Question
- Questions
- questions
- I have questions
- des questions

## intent:main_menu_intent
- main menu
- menu please
- show me the menu
- main menu please
- simba nutri coach menu
- can you please show me the menu
- can you take me to the menu
- menu

## intent:ask_bot_profile_about
- can you please tell me about simbanutricoach
- what is simba nutri coach
- simba nutricoach what's that
- simba nutri coach??
- about_init
- ??
- tell me about simba nutri coach
- about simbanutricoach
- about you

## intent:user_sad_state
- I'm sad
- I'm not feeling well n u
- I'm not okay
- I'm bored
- I'm not fine
- I feel sad Simba
- sad
- bored

## intent:user_happy_state
- I'm fine thanks and you
- I'm fine
- fine n u
- good thanks n u
- I'm great n u

## intent:gen_qtn_what_nutrition
- simba_qtn_01
- what is nutrition
- tell me what nutrition is
- can you please define nutrition to me
- tell me about nutrition
- what the hell is nutrition
- nutrion??
- I wish to know about nutrition
- lishe ni nini?
- niambie lishe ni nini
- unaweza tafadhali kufafanua lishe kwangu
- niambie juu ya lishe
- nini kuzimu ni lishe
- Lishe?
- Natamani kujua juu ya lishe

## intent:gen_qtn_what_balanced_diet
- simba_qtn_02
- what is a balanced diet
- tell me what is a balanced diet
- I don't know about a balanced diet
- balanced diet??
- please educate me about balanced diets
- balanced diet
- balanced diet what's that
- nini lishe bora
- niambie nini lishe bora
- Sijui juu ya lishe bora
- chakula bora??
- tafadhali nifundishe kuhusu lishe bora
- chakula bora
- Lishe bora ni nini hiyo

## intent:gen_qtn_what_malnutrition
- simba_qtn_03
- what is malnutrition
- educate me about malnutrition
- tell me what malnutrition
- what the hell is malnutrition
- malnutrition??
- malnutrition what's is that
- I wish to know more about malnutrition
- utapiamlo
- Nifundishe juu ya utapiamlo
- niambie utapiamlo
- nini kuzimu ni utapiamlo
- utapiamlo?
- utapiamlo ni nini hiyo
- Natamani kujua zaidi juu ya utapiamlo

## intent:gen_qtn_what_malnutrition_forms
- simba_qtn_04
- what are the forms of malnutrition and its effects?
- can you tell me the forms of malnutritions
- forms of malnutrition
- what are the forms of malnutrition
- what are the effects of malnutrition
- please can you explain to me the effects of malnutrition
- how do i know the effects of malnutrition
- tell me the forms and effects of malnutrition
- forms and effects of malnutrition
- Je! ni aina gani ya utapiamlo na athari zake?
- je! unaweza kuniambia aina ya utapiamlo
- aina ya utapiamlo
- ni aina gani za utapiamlo
- nini athari za utapiamlo
- tafadhali unaweza kunielezea athari za utapiamlo
- nawezaje kujua athari za utapiamlo
- niambie aina na athari za utapiamlo
- aina na athari za utapiamlo

## intent:gen_qtn_what_neglected_tropical_disease
- simba_qtn_05
- what are neglected tropical diseases
- neglected tropical diseases?
- can you educate me about tropical diseases
- tell me about neglected tropical diseases
- do you know anything neglected tropical diseases
- I wish to know about neglected tropical diseases
- ni nini kinachopuuzwa magonjwa ya kitropiki
- magonjwa yaliyopuuzwa?
- Je! unaweza kunifundisha juu ya magonjwa ya kitropiki
- niambie juu ya magonjwa yaliyopuuzwa ya kitropiki
- Je! unajua chochote kinachopuuzwa magonjwa ya kitropiki
- Napenda kujua juu ya magonjwa yaliyopuuzwa ya kitropiki

## intent:gen_qtn_what_obesity
- simba_qtn_07
- what is obesity
- can you define obesity for me
- obesity??
- brief me a bit on the subject of obesity
- what the hell is obesity
- explain obesity
- nini fetma
- Je! unaweza kufafanua kunenepa sana kwangu
- fetma ??
- nifupi kidogo juu ya mada ya kunona sana
- nini kuzimu ni fetma
- eleza fetma

## intent:gen_qtn_where_does_obesity
- simba_qtn_08
- where does obesity fit in the nutrition nexus?
- can you tell me how obesity fits in the nutrition nexus
- nutrition and obesity
- can  you explain how obesity fits in the nutrition nexus
- fetma inafaa wapi kwenye nexus ya lishe?
- Je! unaweza kuniambia jinsi fetma inavyofaa kwenye nexus ya lishe
- lishe na fetma
- je! unaweza kuelezea jinsi kunenepa kunakaa kwenye nexus ya lishe

## intent:gen_qtn_what_underfed
- simba_qtn_09
- what is being underfed
- underfed??
- can you tell me being underfed
- educate me about being underfed
- underfeeding what's that
- kinachoendelea
- underfed?
- Je! unaweza kuniambia nikipatwa
- Nifundishe juu ya kupatwa na shida
- kufahamu ni nini hiyo

## intent:gen_qtn_what_overfed
- simba_qtn_10
- what is being overfed
- overfed??
- can you tell me about being overfed
- educate me about being overfed
- overfeeding??
- overfeeding what's that
- kinachozidiwa
- imepitishwa ??
- je! unaweza kuniambia juu ya kupita kiasi
- Nifundishe juu ya kupita kiasi
- overfeeding?
- overfeeding nini hiyo

## intent:gen_qtn_underfed_visit_doc
- simba_qtn_11
- how do i know I am over or under fed without visiting the doctor?
- signs of being underfed
- signs of being overfed
- can i identify that I'm overfed without visiting the doctor
- identify being underfed without visiting the doctor
- identify being under or over fed
- Je! ninajuaje kuwa nimekwisha au nimelishwa bila kutembelea daktari?
- ishara za kupuuzwa
- ishara za kuwa kupita
- naweza kutambua kuwa nimepitwa na pesa bila kumtembelea daktari
- tambua kupuuzwa bila kutembelea daktari
- kubaini kuwa chini au zaidi ya kulishwa

## intent:gen_qtn_how_leverage_tech_height
- simba_qtn_12
- how can i leverage technology to longitudinally track my height and weight?
- how to leverage technology to track height and weight
- health and technology
- how can I track my height using technology
- Ninawezaje kupata teknolojia ya kufuatilia kwa urefu urefu na uzito wangu?
- jinsi ya kuongeza teknolojia ya kufuata urefu na uzito
- afya na teknolojia
- nawezaje kufuata urefu wangu kwa kutumia teknolojia

## intent:gen_qtn_how_family_fight
- simba_qtn_13
- In a family set up how best can i fight malnutrition?
- fighting malnutrition
- can you educate me on how to fight malnutrition
- how best can i fight malnutrition in a family set up
- fighting malnutrition as a family
- the best way to fight malnutrition in a family set up
- Katika familia kuanzisha jinsi bora ninaweza kupigana na utapiamlo?
- kupigana na utapiamlo
- Je! unaweza kunifundisha juu ya jinsi ya kupambana na utapiamlo
- Ninawezaje kupigana na utapiamlo katika familia iliyoanzishwa
- kupigana na utapiamlo kama familia
- njia bora ya kupambana na utapiamlo katika familia iliyowekwa

## intent:gen_qtn_old_suffer
- simba_qtn_14
- can an old person suffer from malnutrition?
- is it possible for a old person to suffer from malnutrition
- what are the chances of suffering from malnutrition if you are old
- old people and malnutrition
- can my grandmother suffer from malnutrition
- can my grandfather suffer from malnutrition
- what are the odds of an elderly person suffering from malnutrition
- Je! mzee anaweza kuugua utapiamlo?
- inawezekana kwa mzee kupata shida ya utapiamlo
- ni nini nafasi za kuteseka kutoka kwa utapiamlo ikiwa umezeeka
- watu wazee na utapiamlo
- Je! bibi yangu anaweza kuugua utapiamlo
- Je! babu yangu anaweza kuugua utapiamlo
- ni nini tabia mbaya ya mtu mzee anayesumbuliwa na utapiamlo

## intent:gen_qtn_recommended_step
- simba_qtn_15
- what are the recommended steps to prevent malnutrition
- malnutrition prevention
- how can i prevent malnutrition
- can you please educate me on the steps to prevent malnutrition
- i want to know how to prevent malnutrition
- prevention of malnutrition??
- ni hatua zipi zinazopendekezwa za kuzuia utapiamlo
- utapiamlo mbaya
- nawezaje kuzuia utapiamlo
- unaweza tafadhali kunifundisha juu ya hatua za kuzuia utapiamlo
- Nataka kujua jinsi ya kuzuia utapiamlo
- kuzuia utapiamlo?

## intent:gen_qtn_days_critical
- simba_qtn_16
- why are the first 1000 days critical in the nutrition space and what do parents need to know?
- what do parents need to know about malnutrition
- which days are very important in the nutrition space
- can you tell me why the 1st 1000 days is critical in the nutrition space
- first 1000 days in the nutrition space 
- what do parents need to know about the criticality of the first 1000 days in nutrition space
- kwa nini siku 1000 za kwanza ni muhimu katika nafasi ya lishe na wazazi wanahitaji kujua nini?
- Je! wazazi wanahitaji kujua nini juu ya utapiamlo
- siku ambazo ni muhimu sana katika nafasi ya lishe
- je! unaweza kuniambia kwa nini siku 1 ya kwanza ni muhimu katika nafasi ya lishe
- siku 1000 za kwanza katika nafasi ya lishe
- Je! wazazi wanahitaji kujua nini juu ya umuhimu wa siku 1000 za kwanza katika nafasi ya lishe

## intent:gen_qtn_how_educate
- simba_qtn_17
- how i can educate my family and community on malnutrition?
- best way to educate people on malnutrition
- teach me how I can educate my family on malnutrition
- I wish to know how to educate the community on malnutrition
- how best can i enlighten the society about malnutrition
- educating family and the general society on malnutrition
- Ninawezaje kuelimisha familia yangu na jamii juu ya utapiamlo?
- njia bora ya kufundisha watu juu ya utapiamlo
- Nifundishe jinsi ninaweza kuelimisha familia yangu juu ya utapiamlo
- Natamani kujua jinsi ya kuelimisha jamii juu ya utapiamlo
- Je! bora naweza kuijaza jamii juu ya utapiamlo
- kuelimisha familia na jamii kwa ujumla juu ya utapiamlo